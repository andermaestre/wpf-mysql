﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AplicacionEscritorioMySQL
{
    /// <summary>
    /// Lógica de interacción para Peticiones.xaml
    /// </summary>
    public partial class Peticiones : UserControl
    {
        public string connectionString;
        public MySqlConnection connection;
        public DataTable dt;
        public Peticiones()
        {
            InitializeComponent();

            
        }
        /// <summary>
        /// Carga los dos grids y el comboBox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            connectionString = "server=" + Properties.Settings.Default.IpString + ";database=database_links;uid=jander;pwd=1234;";
            connection = new MySqlConnection(connectionString);
            connection.Open();
            //Consulta DataGrid
            MySqlCommand cmd = new MySqlCommand("SELECT id, idtrabajador, idetapa FROM peticiones", connection);
            dt = new DataTable();
            dt.Load(cmd.ExecuteReader());

            //Consulta ComboBox
            MySqlCommand cmd2 = new MySqlCommand("SELECT id, nombre, apellidos FROM trabajadores", connection);
            DataTable dt2 = new DataTable();
            dt2.Load(cmd2.ExecuteReader());

            //Consulta Etapas
            MySqlCommand cmd3 = new MySqlCommand("SELECT id, titulo, descripcion FROM etapas", connection);
            DataTable dt3 = new DataTable();
            dt3.Load(cmd3.ExecuteReader());

            connection.Close();

            //Carga datagrid
            dtGrid.DataContext = dt;

            //Carga ComboBox
            for (int i = 0; i < dt2.Rows.Count; i++)
            {
                cbTrabajadores.Items.Add(dt2.Rows[i]["id"] + "- " + dt2.Rows[i]["nombre"] + " " + dt2.Rows[i]["apellidos"]);
            }

            //Carga DataGrid Etapas
            dtgEtapas.DataContext = dt3;
        }
        /// <summary>
        /// Al elegir en el ComboBox vemos las peticiones de ese señor
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CbTrabajadores_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string seleccion = cbTrabajadores.SelectedValue.ToString();
            string[] idSplit = seleccion.Split('-');
            
            int idtrab = int.Parse(idSplit[0].ToString());


            connection.Open();
            //Consulta DataGrid
            MySqlCommand cmd = new MySqlCommand("SELECT * FROM peticiones WHERE idtrabajador = @idtrabajador", connection);
            MySqlParameter mySqlParameter = new MySqlParameter();
            DataTable dtpet = new DataTable();
            mySqlParameter.ParameterName = "@idtrabajador";
            mySqlParameter.Value = idtrab;
            cmd.Parameters.Add(mySqlParameter);
                
            dtpet.Load(cmd.ExecuteReader());
            connection.Close();
            dt = dtpet;
            
            if(dt.Rows.Count == 0)
            {
                dtGrid.DataContext = null;
                MessageBox.Show("No hay peticiones de este trabajador.");
                
            }
            else
            {
                dtGrid.DataContext = dt;
            }

            connection.Close();
        }
        
        private void BtnAceptar_Click(object sender, RoutedEventArgs e)
        {
            int elegido = dtGrid.SelectedIndex;
            if (elegido != -1)
            {
                //Preparacion de consultas
                int idtrabajador = int.Parse(dt.Rows[elegido]["idtrabajador"].ToString());
                int idetapa = int.Parse(dt.Rows[elegido]["idtrabajador"].ToString());
                MySqlCommand cmd = new MySqlCommand("INSERT INTO trabajadores_etapa (idtrabajador, idetapa) VALUES(@idtrabajador, @idetapa)", connection);


                MySqlParameter mySqlParameter = new MySqlParameter();
                mySqlParameter.ParameterName = "@idtrabajador";
                mySqlParameter.Value = idtrabajador;
                cmd.Parameters.Add(mySqlParameter);

                MySqlParameter mySqlParameter2 = new MySqlParameter();
                mySqlParameter2.ParameterName = "@idetapa";
                mySqlParameter2.Value = idetapa;
                cmd.Parameters.Add(mySqlParameter2);



                MySqlCommand cmd4 = new MySqlCommand("DELETE FROM Peticiones WHERE id = @idborra", connection);
                MySqlParameter mySqlParameter4 = new MySqlParameter();
                mySqlParameter4.ParameterName = "@idborra";
                mySqlParameter4.Value = int.Parse(dt.Rows[elegido]["id"].ToString());
                cmd4.Parameters.Add(mySqlParameter4);


                MySqlCommand cmd2 = new MySqlCommand("Select horasacum, horasmax from trabajadores where id=@id", connection);
                MySqlParameter mySqlParameter3 = new MySqlParameter();
                mySqlParameter3.ParameterName = "@id";
                mySqlParameter3.Value = idtrabajador;
                cmd2.Parameters.Add(mySqlParameter3);

                MySqlCommand cmd3 = new MySqlCommand("Select horas from etapas where id=@idetapa2", connection);
                MySqlParameter mySqlParameter5 = new MySqlParameter();
                mySqlParameter5.ParameterName = "@idetapa2";
                mySqlParameter5.Value = idetapa;
                cmd3.Parameters.Add(mySqlParameter5);

                DataTable dt2 = new DataTable();
                DataTable dt3 = new DataTable();
                //Consulto horas del trabajador y de la etapa
                connection.Open();
                dt2.Load(cmd2.ExecuteReader());
                dt3.Load(cmd3.ExecuteReader());
                
                
                connection.Close();
                if(dt3.Rows.Count == 0)
                {
                    MessageBox.Show("ya no existe la etapa.");
                    return;
                }
                int horasetapa = int.Parse(dt3.Rows[0]["horas"].ToString());
                int horastrab = int.Parse(dt2.Rows[0]["horasacum"].ToString());
                int horasmax = int.Parse(dt2.Rows[0]["horasacum"].ToString());
                int total = horasetapa + horastrab;
                
                MessageBoxResult rst;

                //Confirmo
                if (total > horasmax)
                {
                    rst = MessageBox.Show("Se pasa de horas. Continuar?", "Socorro", MessageBoxButton.YesNo);
                }
                else
                {
                    rst = MessageBox.Show("Todo ok. Continuar?", "OK", MessageBoxButton.YesNo);
                }

                if (rst == MessageBoxResult.Yes)
                {

                    MySqlCommand cmd5 = new MySqlCommand("UPDATE trabajadores SET horasacum=@horas WHERE id = @id", connection);
                    MySqlParameter mySqlParameter6 = new MySqlParameter();
                    mySqlParameter6.ParameterName = "@horas";
                    mySqlParameter6.Value = idtrabajador;
                    cmd5.Parameters.Add(mySqlParameter6);
                    //Insert en la tabla trabajadores_etapa
                    cmd.ExecuteNonQuery();
                    //Delete de peticiones
                    cmd4.ExecuteNonQuery();
                    //Update de las horas del trabajador
                    cmd5.ExecuteNonQuery();
                }

                connection.Close();
            }
            
            
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            int elegido = dtGrid.SelectedIndex;
            int id = int.Parse(dt.Rows[elegido]["id"].ToString());

            if (elegido != -1)
            {
                MySqlCommand cmd = new MySqlCommand("DELETE FROM peticiones WHERE id=@id", connection);
                MySqlParameter mySqlParameter = new MySqlParameter();
                mySqlParameter.ParameterName = "@id";
                mySqlParameter.Value = id;
                cmd.Parameters.Add(mySqlParameter);

                MySqlCommand cmd2 = new MySqlCommand("SELECT id, idtrabajador, idetapa FROM peticiones", connection);
                DataTable dtaux = new DataTable();

                connection.Open();
                dtaux.Load(cmd2.ExecuteReader());
                cmd.ExecuteNonQuery();
                connection.Close();

                dt = dtaux;

                dtGrid.DataContext = null;
                dtGrid.DataContext = dt;

            }
        }
    }
}
