﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Configuration;
/// <summary>
/// Buen intento pero no llego
/// </summary>
namespace AplicacionEscritorioMySQL
{
    /// <summary>
    /// Lógica de interacción para IpControl.xaml
    /// </summary>
    public partial class IpControl : UserControl
    {
        public IpControl()
        {
            InitializeComponent();
        }

        
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            txtIp.Text = Properties.Settings.Default.IpString;
            
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.IpString = txtIp.Text;
            Properties.Settings.Default.Save();
            lblSuccess.Visibility = Visibility.Visible;
        }

        private void IpUserControl_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            lblSuccess.Visibility = Visibility.Collapsed;
        }
    }
}
