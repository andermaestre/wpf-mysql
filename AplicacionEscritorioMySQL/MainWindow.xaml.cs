﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AplicacionEscritorioMySQL
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Variables de conexion
        /// </summary>
        public string connectionString;
        MySqlConnection connection;

        public MainWindow()
        {
            Form1 f = new Form1();
            f.ShowDialog();
            InitializeComponent();
            //Inicializo conexion
            connectionString = "server=" + Properties.Settings.Default.IpString + ";database=database_links;uid=jander;pwd=1234;";
            connection = new MySqlConnection(connectionString);

        }

        private void ListViewItem_MouseEnter(object sender, MouseEventArgs e)
        {
            //Estilos del menu

            if (Btn1.IsChecked == true)
            {
                tt_Home.Visibility = Visibility.Collapsed;
                Peticiones.Visibility = Visibility.Collapsed;
                Trabajadores.Visibility = Visibility.Collapsed;
                Etapas.Visibility = Visibility.Collapsed;
                Procesos.Visibility = Visibility.Collapsed;
                IP.Visibility = Visibility.Collapsed;
                
            }
            else
            {
                tt_Home.Visibility = Visibility.Visible;
                Peticiones.Visibility = Visibility.Visible;
                Trabajadores.Visibility = Visibility.Visible;
                Etapas.Visibility = Visibility.Visible;
                Procesos.Visibility = Visibility.Visible;
                IP.Visibility = Visibility.Visible;
                
            }
        }

        private void Btn1_Unchecked(object sender, RoutedEventArgs e)
        {
            img_bg.Opacity = 1;
        }

        private void Btn1_Checked(object sender, RoutedEventArgs e)
        {
            img_bg.Opacity = 0.3;
        }

        private void BG_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Btn1.IsChecked = false;
        }
        //Boton de cerrar
        private void CloseBtn_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
        
        /// <summary>
        /// Funcion para esconder paneles y enseñar el elegido
        /// </summary>
        /// <param name="control"></param>
        public void SetActiveUserControl (UserControl control)
        {
            esconderPaneles();

            control.Visibility = Visibility.Visible;
        }
        /// <summary>
        /// Funcion esconder todo
        /// </summary>
        private void esconderPaneles()
        {
            pnlPeticiones.Visibility = Visibility.Collapsed;
            pnlIp.Visibility = Visibility.Collapsed;
            pnlTrabajadores.Visibility = Visibility.Collapsed;
            pnlEtapas.Visibility = Visibility.Collapsed;
            pnlProcesos.Visibility = Visibility.Collapsed;
        }

        /// <summary>
        /// Ejemplo de funcionalidad de un boton
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnPeticiones_Click(object sender, RoutedEventArgs e)
        {
            SetActiveUserControl(pnlPeticiones);
        }
        //Igual
        private void BtnIp_Click(object sender, RoutedEventArgs e)
        {
            SetActiveUserControl(pnlIp);
        }
        /// <summary>
        /// Home solo esconde
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnHome_Click(object sender, RoutedEventArgs e)
        {
            esconderPaneles();
        }
        //Igual
        private void BtnTrabajadores_Click(object sender, RoutedEventArgs e)
        {
            SetActiveUserControl(pnlTrabajadores);
        }
        //Igual
        private void BtnEtapas_Click(object sender, RoutedEventArgs e)
        {
            SetActiveUserControl(pnlEtapas);
        }
        //Igual
        private void BtnProcesos_Click(object sender, RoutedEventArgs e)
        {
            SetActiveUserControl(pnlProcesos);
        }
    }
}
