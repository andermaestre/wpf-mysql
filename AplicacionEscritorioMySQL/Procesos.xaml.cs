﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AplicacionEscritorioMySQL
{
    /// <summary>
    /// Lógica de interacción para Procesos.xaml
    /// </summary>
    public partial class Procesos : UserControl
    {
        public string connectionString;
        public MySqlConnection connection;
        DataTable dt;
        public Procesos()
        {
            InitializeComponent();
        }


        /// <summary>
        /// Inicializar mis componentes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            connectionString = "server=" + Properties.Settings.Default.IpString + ";database=database_links;uid=jander;pwd=1234;";
            connection = new MySqlConnection(connectionString);
            connection.Open();
            //Consulta DataGrid
            MySqlCommand cmd = new MySqlCommand("SELECT id, titulo, descripcion FROM procesos", connection);
            dt = new DataTable();
            dt.Load(cmd.ExecuteReader());
            connection.Close();

            dtgProcesos.DataContext = dt;

        }
        /// <summary>
        /// Selecciona todas las etapas de ese proceso y las visualiza
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DtgProcesos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int elegido = dtgProcesos.SelectedIndex;
            if(elegido != -1)
            {
                int idproceso = int.Parse(dt.Rows[elegido]["id"].ToString());
                MessageBox.Show(elegido.ToString() + " - " + idproceso.ToString());

                connection.Open();
                //Consulta DataGrid
                MySqlCommand cmd = new MySqlCommand("Select * FROM etapas Where id IN (SELECT idetapa FROM etapas_proceso WHERE idproceso = @idproceso)", connection);
                MySqlParameter mySqlParameter = new MySqlParameter();
                mySqlParameter.ParameterName = "@idproceso";
                mySqlParameter.Value = idproceso;
                cmd.Parameters.Add(mySqlParameter);
                DataTable dt2 = new DataTable();
                dt2.Load(cmd.ExecuteReader());
                connection.Close();

                dtgEtapas.DataContext = dt2;
            }
            
        }
        /// <summary>
        /// Elimina el campo seleccionado
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnEliminarPro_Click(object sender, RoutedEventArgs e)
        {
            int elegido = dtgProcesos.SelectedIndex;

            int idproceso = int.Parse(dt.Rows[elegido]["id"].ToString());
            MessageBox.Show(elegido.ToString() + " - " + idproceso.ToString());

            MessageBoxResult rst = MessageBox.Show("Vas a eliminar el proceso " + dt.Rows[elegido]["titulo"].ToString() + " y sus asignaciones\n Estas seguro?", "Lo borras del mapa", MessageBoxButton.YesNo);
            if(rst == MessageBoxResult.Yes)
            {
                
                //Consulta DataGrid
                MySqlCommand cmd = new MySqlCommand("DELETE From etapas_proceso Where idproceso = @idproceso", connection);
                MySqlParameter mySqlParameter = new MySqlParameter();
                mySqlParameter.ParameterName = "@idproceso";
                mySqlParameter.Value = idproceso;
                cmd.Parameters.Add(mySqlParameter);

                MySqlCommand cmd2 = new MySqlCommand("DELETE From procesos Where id = @id", connection);
                MySqlParameter mySqlParameter2 = new MySqlParameter();
                mySqlParameter2.ParameterName = "@id";
                mySqlParameter2.Value = idproceso;
                cmd2.Parameters.Add(mySqlParameter2);

                MySqlCommand cmd3 = new MySqlCommand("SELECT id, titulo, descripcion FROM procesos", connection);
                DataTable dtaux = new DataTable();

                connection.Open();
                cmd.ExecuteNonQuery();
                dtaux.Load(cmd3.ExecuteReader());
                cmd2.ExecuteNonQuery();
                connection.Close();
                dt = dtaux;
                dtgProcesos.DataContext = dt;
            }
        }
        /// <summary>
        /// Habilita los controles
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnNuevo_Click(object sender, RoutedEventArgs e)
        {
            txtTitulo.IsEnabled = true;
            txtDesc.IsEnabled = true;
            btnAceptar.Visibility = Visibility.Visible;
            btnAceptar.IsEnabled = true;
            btnEliminarPro.IsEnabled = false;
            btnEditar.IsEnabled = false;
            btnNuevo.IsEnabled = false;
        }
        /// <summary>
        /// Insert de los datos en los campos de edicion
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnAceptar_Click(object sender, RoutedEventArgs e)
        {
            string titulo = txtTitulo.Text;
            string descripcion = txtDesc.Text;

            MySqlCommand cmd = new MySqlCommand("INSERT INTO procesos (titulo, descripcion) VALUES (@titulo, @desc)", connection);
            MySqlParameter mySqlParameter = new MySqlParameter();
            mySqlParameter.ParameterName = "@titulo";
            mySqlParameter.Value = titulo;
            cmd.Parameters.Add(mySqlParameter);

            MySqlParameter mySqlParameter2 = new MySqlParameter();
            mySqlParameter2.ParameterName = "@desc";
            mySqlParameter2.Value = descripcion;
            cmd.Parameters.Add(mySqlParameter2);

            MySqlCommand cmd2 = new MySqlCommand("SELECT id, titulo, descripcion FROM procesos", connection);
            DataTable dtaux = new DataTable();
            

            connection.Open();
            cmd.ExecuteNonQuery();
            dtaux.Load(cmd2.ExecuteReader());
            connection.Close();
            dt = dtaux;
            dtgProcesos.DataContext = dt;
            txtTitulo.IsEnabled = false;
            txtDesc.IsEnabled = false;
            btnAceptar.Visibility = Visibility.Collapsed;
            btnAceptar.IsEnabled = false;
            btnEliminarPro.IsEnabled = true;
            btnEditar.IsEnabled = true;
            btnNuevo.IsEnabled = true;
        }

        /// <summary>
        /// Carga la informacion del campo seleccionado y habilita los controles
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnEditar_Click(object sender, RoutedEventArgs e)
        {
            txtTitulo.IsEnabled = true;
            txtDesc.IsEnabled = true;
            btnAceptar2.Visibility = Visibility.Visible;
            btnAceptar2.IsEnabled = true;
            btnEliminarPro.IsEnabled = false;
            btnEditar.IsEnabled = false;
            btnNuevo.IsEnabled = false;

            int elegido = dtgProcesos.SelectedIndex;
            
            int idproceso = int.Parse(dt.Rows[elegido]["id"].ToString());
            lblId.Content = idproceso.ToString();
            MessageBox.Show(elegido.ToString() + " - " + idproceso.ToString());

            MySqlCommand cmd = new MySqlCommand("SELECT titulo, descripcion FROM procesos WHERE id = @id", connection);
            MySqlParameter mySqlParameter = new MySqlParameter();
            mySqlParameter.ParameterName = "@id";
            mySqlParameter.Value = idproceso;
            cmd.Parameters.Add(mySqlParameter);
            DataTable dt2 = new DataTable();

            connection.Open();
            dt2.Load(cmd.ExecuteReader());
            connection.Close();

            txtTitulo.Text = dt2.Rows[0]["titulo"].ToString();
            txtDesc.Text = dt2.Rows[0]["descripcion"].ToString();

        }
        /// <summary>
        /// Confirma los cambios en la informacion de los controles
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnAceptar2_Click(object sender, RoutedEventArgs e)
        {
            string titulo = txtTitulo.Text;
            string descripcion = txtDesc.Text;

            MySqlCommand cmd = new MySqlCommand("UPDATE procesos SET titulo = @tit, descripcion = @desc WHERE id = @id", connection);
            MySqlParameter mySqlParameter = new MySqlParameter();
            mySqlParameter.ParameterName = "@tit";
            mySqlParameter.Value = titulo;
            cmd.Parameters.Add(mySqlParameter);

            MySqlParameter mySqlParameter2 = new MySqlParameter();
            mySqlParameter2.ParameterName = "@desc";
            mySqlParameter2.Value = descripcion;
            cmd.Parameters.Add(mySqlParameter2);

            MySqlParameter mySqlParameter3 = new MySqlParameter();
            mySqlParameter3.ParameterName = "@id";
            mySqlParameter3.Value = int.Parse(lblId.Content.ToString());
            cmd.Parameters.Add(mySqlParameter3);

            //Consulta DataGrid otra vez
            MySqlCommand cmd2 = new MySqlCommand("SELECT id, titulo, descripcion FROM procesos", connection);
            DataTable dtaux = new DataTable();
            
            connection.Open();
            cmd.ExecuteNonQuery();
            
            dtaux.Load(cmd2.ExecuteReader());
            connection.Close();

            dt = dtaux;
            dtgProcesos.DataContext = dt;
            
            txtTitulo.IsEnabled = false;
            txtDesc.IsEnabled = false;
            btnAceptar2.Visibility = Visibility.Collapsed;
            btnAceptar2.IsEnabled = false;
            btnEliminarPro.IsEnabled = true;
            btnEditar.IsEnabled = true;
            btnNuevo.IsEnabled = true;
        }
    }
}
