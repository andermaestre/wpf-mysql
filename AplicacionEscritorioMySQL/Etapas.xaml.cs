﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AplicacionEscritorioMySQL
{
    
    public partial class Etapas : UserControl
    {
        public string connectionString;
        public MySqlConnection connection;
        public DataTable dt;
        public Etapas()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Conexion y carga del datagrid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            connectionString = "server=" + Properties.Settings.Default.IpString + ";database=database_links;uid=jander;pwd=1234;";
            connection = new MySqlConnection(connectionString);
            connection.Open();
            //Consulta DataGrid
            MySqlCommand cmd = new MySqlCommand("SELECT id, titulo, descripcion, horas FROM etapas", connection);
            dt = new DataTable();
            dt.Load(cmd.ExecuteReader());

            connection.Close();

            //Carga datagrid
            dtgEtapas.DataContext = dt;
        }
        /// <summary>
        /// OnSelectionChanged del panel principal cambia el secundario
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DtgEtapas_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int elegido = dtgEtapas.SelectedIndex;
            if (elegido != -1)
            {
                int id = int.Parse(dt.Rows[elegido]["id"].ToString());

                string titulo = dt.Rows[elegido]["titulo"].ToString();
                string descripcion = dt.Rows[elegido]["descripcion"].ToString();
                int horas = int.Parse(dt.Rows[elegido]["horas"].ToString());

                MySqlCommand cmd = new MySqlCommand("SELECT id, nombre, apellidos FROM trabajadores WHERE id IN (Select idtrabajador from trabajadores_etapa Where idetapa=@idetapa)", connection);

                MySqlParameter mySqlParameter = new MySqlParameter();
                mySqlParameter.ParameterName = "@idetapa";
                mySqlParameter.Value = id;
                cmd.Parameters.Add(mySqlParameter);

                DataTable dt2 = new DataTable();
                connection.Open();
                dt2.Load(cmd.ExecuteReader());
                connection.Close();

                dtgTrabajadoresEtapa.DataContext = dt2;
            }
            else
            {
                dtgTrabajadoresEtapa.DataContext = null; 
            }
            
        }

        private void BtnEliminar_Click(object sender, RoutedEventArgs e)
        {
            int elegido = dtgEtapas.SelectedIndex;
            if (elegido != -1)
            {
                int id = int.Parse(dt.Rows[elegido]["id"].ToString());

                MessageBoxResult resp = MessageBox.Show("Vas a borrar " + dt.Rows[elegido]["titulo"].ToString() + "\n Estas seguro machote?", "Alerta de fusion", MessageBoxButton.YesNo);
                if (resp == MessageBoxResult.Yes)
                {
                    MySqlCommand cmd = new MySqlCommand("DELETE FROM etapas WHERE id = @id", connection);
                    MySqlCommand cmd2 = new MySqlCommand("DELETE FROM trabajadores_etapa WHERE idetapa = @id", connection);
                    MySqlCommand cmd3 = new MySqlCommand("SELECT id, titulo, descripcion, horas FROM etapas", connection);

                    DataTable dtaux = new DataTable();

                    MySqlParameter mySqlParameter = new MySqlParameter();
                    MySqlParameter mySqlParameter2 = new MySqlParameter();
                    mySqlParameter.ParameterName = "@id";
                    mySqlParameter2.ParameterName = "@id";
                    mySqlParameter.Value = id;
                    mySqlParameter2.Value = id;
                    cmd.Parameters.Add(mySqlParameter);
                    cmd2.Parameters.Add(mySqlParameter2);

                    connection.Open();

                    cmd.ExecuteNonQuery();
                    cmd2.ExecuteNonQuery();
                    dtaux.Load(cmd3.ExecuteReader());

                    connection.Close();

                    dt = dtaux;
                    dtgEtapas.DataContext = null;
                    dtgEtapas.DataContext = dt;

                    dtgTrabajadoresEtapa.DataContext = null;

                }
                else
                {
                    MessageBox.Show("Que vas a borrar si no eliges?");
                }
            }
            
        }
        /// <summary>
        /// Consulta en la base de datos los campos a editar y los coloca
        /// maneja la visibilidad de los controles
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnEditar_Click(object sender, RoutedEventArgs e)
        {
            


            int elegido = dtgEtapas.SelectedIndex;
            if (elegido != -1)
            {
                txtTitulo.IsEnabled = true;
                txtHoras.IsEnabled = true;
                txtDesc.IsEnabled = true;

                btnAceptar.Visibility = Visibility.Visible;

                btnEditar.IsEnabled = false;
                btnEliminar.IsEnabled = false;
                btnNuevo.IsEnabled = false;

                int id = int.Parse(dt.Rows[elegido]["id"].ToString());
                lblId.Content = id;
                MySqlCommand cmd = new MySqlCommand("SELECT * FROM etapas WHERE id = @id", connection);

                MySqlParameter mySqlParameter = new MySqlParameter();
                mySqlParameter.ParameterName = "@id";
                mySqlParameter.Value = id;
                cmd.Parameters.Add(mySqlParameter);

                DataTable dt2 = new DataTable();

                connection.Open();
                dt2.Load(cmd.ExecuteReader());
                connection.Close();
                txtTitulo.Text = dt2.Rows[0]["titulo"].ToString();
                txtHoras.Text = dt2.Rows[0]["horas"].ToString();
                txtDesc.Text = dt2.Rows[0]["descripcion"].ToString();
            }
            else
            {
                MessageBox.Show("Elige");
            }
            

        }
        /// <summary>
        /// Funcion de aceptar la edicion
        /// Maneja los controles a la inversa
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnAceptar_Click(object sender, RoutedEventArgs e)
        {
            string titulo = txtTitulo.Text;
            string descripcion = txtDesc.Text;
            int horas = int.Parse(txtHoras.Text);

            MySqlCommand cmd = new MySqlCommand("UPDATE etapas SET titulo=@tit, horas= @horas, descripcion=@desc WHERE id = @id", connection);

            MySqlParameter mySqlParameter = new MySqlParameter();
            mySqlParameter.ParameterName = "@id";
            mySqlParameter.Value = int.Parse(lblId.Content.ToString());
            cmd.Parameters.Add(mySqlParameter);

            MySqlParameter mySqlParameter2 = new MySqlParameter();
            mySqlParameter2.ParameterName = "@tit";
            mySqlParameter2.Value = titulo;
            cmd.Parameters.Add(mySqlParameter2);

            MySqlParameter mySqlParameter3 = new MySqlParameter();
            mySqlParameter3.ParameterName = "@horas";
            mySqlParameter3.Value = horas;
            cmd.Parameters.Add(mySqlParameter3);

            MySqlParameter mySqlParameter4 = new MySqlParameter();
            mySqlParameter4.ParameterName = "@desc";
            mySqlParameter4.Value = descripcion;
            cmd.Parameters.Add(mySqlParameter4);

            MySqlCommand cmd3 = new MySqlCommand("SELECT id, titulo, descripcion, horas FROM etapas", connection);
            DataTable dtaux = new DataTable();

            connection.Open();
            cmd.ExecuteNonQuery();
            dtaux.Load(cmd3.ExecuteReader());
            connection.Close();

            dt = dtaux;
            dtgEtapas.DataContext = dt;

            txtTitulo.IsEnabled = false;
            txtHoras.IsEnabled = false;
            txtDesc.IsEnabled = false;

            btnAceptar.Visibility = Visibility.Collapsed;

            btnEditar.IsEnabled = true;
            btnEliminar.IsEnabled = true;
            btnNuevo.IsEnabled = true;
        }
        /// <summary>
        /// Habilita los campos de edicion
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnNuevo_Click(object sender, RoutedEventArgs e)
        {
            txtTitulo.IsEnabled = true;
            txtHoras.IsEnabled = true;
            txtDesc.IsEnabled = true;

            btnAceptar2.Visibility = Visibility.Visible;

            btnEditar.IsEnabled = false;
            btnEliminar.IsEnabled = false;
            btnNuevo.IsEnabled = false;
        }
        /// <summary>
        /// Funcion de aceptar nuevo
        /// Inserta en la base de datos y maneja controles
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnAceptar2_Click(object sender, RoutedEventArgs e)
        {
            string titulo = txtTitulo.Text;
            string descripcion = txtDesc.Text;
            int horas = int.Parse(txtHoras.Text);

            MySqlCommand cmd = new MySqlCommand("INSERT INTO etapas (titulo,horas,descripcion) VALUES (@tit, @horas, @desc)", connection);
                    
            MySqlParameter mySqlParameter2 = new MySqlParameter();
            mySqlParameter2.ParameterName = "@tit";
            mySqlParameter2.Value = titulo;
            cmd.Parameters.Add(mySqlParameter2);

            MySqlParameter mySqlParameter3 = new MySqlParameter();
            mySqlParameter3.ParameterName = "@horas";
            mySqlParameter3.Value = horas;
            cmd.Parameters.Add(mySqlParameter3);

            MySqlParameter mySqlParameter4 = new MySqlParameter();
            mySqlParameter4.ParameterName = "@desc";
            mySqlParameter4.Value = descripcion;
            cmd.Parameters.Add(mySqlParameter4);

            MySqlCommand cmd3 = new MySqlCommand("SELECT id, titulo, descripcion, horas FROM etapas", connection);
            DataTable dtaux = new DataTable();

            connection.Open();
            cmd.ExecuteNonQuery();
            dtaux.Load(cmd3.ExecuteReader());
            connection.Close();

            dt = dtaux;
            dtgEtapas.DataContext = null;
            dtgEtapas.DataContext = dt;

            txtTitulo.IsEnabled = false;
            txtHoras.IsEnabled = false;
            txtDesc.IsEnabled = false;

            btnAceptar.Visibility = Visibility.Collapsed;

            btnEditar.IsEnabled = true;
            btnEliminar.IsEnabled = true;
            btnNuevo.IsEnabled = true;
        }
    }
}
