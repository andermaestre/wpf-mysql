﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace AplicacionEscritorioMySQL
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            string ip = txtIp.Text;

            Properties.Settings.Default.IpString = ip;
            Properties.Settings.Default.Save();

            MessageBox.Show("La ip es " + Properties.Settings.Default.IpString);

            this.Close();

        }

        private void txtIp_Enter(object sender, EventArgs e)
        {
            string ip = txtIp.Text;

            Properties.Settings.Default.IpString = ip;
            Properties.Settings.Default.Save();

            MessageBox.Show("La ip es " + Properties.Settings.Default.IpString);

            this.Close();
        }


    }
}
