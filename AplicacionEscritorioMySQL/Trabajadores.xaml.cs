﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AplicacionEscritorioMySQL
{
    /// <summary>
    /// Lógica de interacción para Trabajadores.xaml
    /// </summary>
    public partial class Trabajadores : UserControl
    {
        public string connectionString;
        public MySqlConnection connection;
        DataTable dt;
        int state;
        public Trabajadores()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Carga el datagrid con la informacion de los trabajadores
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TrabajadoresUserControl_Loaded(object sender, RoutedEventArgs e)
        {
            connectionString = "server=" + Properties.Settings.Default.IpString + ";database=database_links;uid=jander;pwd=1234;";
            connection = new MySqlConnection(connectionString);
            connection.Open();
            //Consulta DataGrid
            MySqlCommand cmd = new MySqlCommand("SELECT id, nombre, apellidos, usuario, horasacum, horasmax FROM trabajadores", connection);
            dt = new DataTable();
            dt.Load(cmd.ExecuteReader());
            connection.Close();

            dtgTrabajadores.DataContext = dt;
            state = -1;
        }
        /// <summary>
        /// Boton cambiar nombre
        /// Carga los datos y los muestra en los editText
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            int electo = dtgTrabajadores.SelectedIndex;
            if (electo != -1)
            {
                txt1.Visibility = Visibility.Visible;
                txt2.Visibility = Visibility.Visible;

                btnAceptar.Visibility = Visibility.Visible;
                btnHoras.Visibility = Visibility.Collapsed;
                btnNombre.Visibility = Visibility.Collapsed;
                btnUsuario.Visibility = Visibility.Collapsed;
                int idtrabajador = int.Parse(dt.Rows[electo]["id"].ToString());
                MySqlCommand cmd = new MySqlCommand("SELECT nombre, apellidos FROM trabajadores WHERE id = @idtrab", connection);

                MySqlParameter mySqlParameter = new MySqlParameter();
                mySqlParameter.ParameterName = "@idtrab";
                mySqlParameter.Value = idtrabajador;
                cmd.Parameters.Add(mySqlParameter);

                DataTable dt2 = new DataTable();

                connection.Open();
                dt2.Load(cmd.ExecuteReader());
                connection.Close();

                txt1.Text = dt2.Rows[0]["nombre"].ToString();
                txt2.Text = dt2.Rows[0]["apellidos"].ToString();
                state = 1;
                lblId.Content = idtrabajador;
            }
            else
            {
                MessageBox.Show("Elige uno hombre!");
            }
        }
        /// <summary>
        /// Igual que la anterior pero con las horas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnHoras_Click(object sender, RoutedEventArgs e)
        {
            

            int electo = dtgTrabajadores.SelectedIndex;
            if(electo != -1)
            {
                txt1.Visibility = Visibility.Visible;
                txt2.Visibility = Visibility.Visible;

                btnAceptar.Visibility = Visibility.Visible;
                btnHoras.Visibility = Visibility.Collapsed;
                btnNombre.Visibility = Visibility.Collapsed;
                btnUsuario.Visibility = Visibility.Collapsed;
                int idtrabajador = int.Parse(dt.Rows[electo]["id"].ToString());
                MySqlCommand cmd = new MySqlCommand("SELECT horasmax, horasacum FROM trabajadores WHERE id =@idtrab", connection);

                MySqlParameter mySqlParameter = new MySqlParameter();
                mySqlParameter.ParameterName = "@idtrab";
                mySqlParameter.Value = idtrabajador;
                cmd.Parameters.Add(mySqlParameter);

                DataTable dt2 = new DataTable();

                connection.Open();
                dt2.Load(cmd.ExecuteReader());
                connection.Close();

                txt1.Text = dt2.Rows[0]["horasacum"].ToString();
                txt2.Text = dt2.Rows[0]["horasmax"].ToString();
                state = 2;
                lblId.Content = idtrabajador;
            }
            else
            {
                MessageBox.Show("Elige uno hombre!");
            }
            
        }
        /// <summary>
        /// Lo mismo pero con el user
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnUsuario_Click(object sender, RoutedEventArgs e)
        {
            
            //Miro elegido
            int electo = dtgTrabajadores.SelectedIndex;
            if (electo != -1)
            {
                //Visibilidad de controles
                txt1.Visibility = Visibility.Visible;
                btnAceptar.Visibility = Visibility.Visible;
                btnHoras.Visibility = Visibility.Collapsed;
                btnNombre.Visibility = Visibility.Collapsed;
                btnUsuario.Visibility = Visibility.Collapsed;
                //Captura de id elegido
                int idtrabajador = int.Parse(dt.Rows[electo]["id"].ToString());
                //Preparacion de consulta
                MySqlCommand cmd = new MySqlCommand("SELECT usuario FROM trabajadores WHERE id =@idtrab", connection);
                //Con parametro
                MySqlParameter mySqlParameter = new MySqlParameter();
                mySqlParameter.ParameterName = "@idtrab";
                mySqlParameter.Value = idtrabajador;
                cmd.Parameters.Add(mySqlParameter);
                //Nueva datatable
                DataTable dt2 = new DataTable();

                connection.Open();
                //Y cargar
                dt2.Load(cmd.ExecuteReader());
                connection.Close();
                //Mostrar informacion
                txt1.Text = dt2.Rows[0]["usuario"].ToString();
                state = 3;
                lblId.Content = idtrabajador;
            }
            else
            {
                MessageBox.Show("Elige uno hombre!");
            }
        }
        /// <summary>
        /// Confirma con elegancia en cualquier caso
        /// y controles otra vez
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnAceptar_Click(object sender, RoutedEventArgs e)
        {
            string text1 = txt1.Text;
            string text2 = txt2.Text;


            switch (state)
            {
                case 1:
                    MySqlCommand cmd = new MySqlCommand("UPDATE trabajadores SET nombre=@nom, apellidos= @ape WHERE id = @id", connection);

                    MySqlParameter mySqlParameter = new MySqlParameter();
                    mySqlParameter.ParameterName = "@nom";
                    mySqlParameter.Value = text1;
                    cmd.Parameters.Add(mySqlParameter);

                    MySqlParameter mySqlParameter2 = new MySqlParameter();
                    mySqlParameter2.ParameterName = "@ape";
                    mySqlParameter2.Value = text2;
                    cmd.Parameters.Add(mySqlParameter2);

                    MySqlParameter mySqlParameter3 = new MySqlParameter();
                    mySqlParameter3.ParameterName = "@id";
                    mySqlParameter3.Value = int.Parse(lblId.Content.ToString());
                    cmd.Parameters.Add(mySqlParameter3);
                    connection.Open();
                    cmd.ExecuteNonQuery();

                    break;
                case 2:
                    MySqlCommand cmd2 = new MySqlCommand("UPDATE trabajadores SET horasacum=@acu, horasmax= @max WHERE id = @id2", connection);

                    MySqlParameter mySqlParameter4 = new MySqlParameter();
                    mySqlParameter4.ParameterName = "@acu";
                    mySqlParameter4.Value = text1;
                    cmd2.Parameters.Add(mySqlParameter4);

                    MySqlParameter mySqlParameter5 = new MySqlParameter();
                    mySqlParameter5.ParameterName = "@max";
                    mySqlParameter5.Value = text2;
                    cmd2.Parameters.Add(mySqlParameter5);

                    MySqlParameter mySqlParameter6 = new MySqlParameter();
                    mySqlParameter6.ParameterName = "@id2";
                    mySqlParameter6.Value = int.Parse(lblId.Content.ToString());
                    cmd2.Parameters.Add(mySqlParameter6);
                    connection.Open();
                    cmd2.ExecuteNonQuery();
                    break;
                case 3:
                    MySqlCommand cmd3 = new MySqlCommand("UPDATE trabajadores SET usuario=@usu WHERE id = @id3", connection);

                    MySqlParameter mySqlParameter7 = new MySqlParameter();
                    mySqlParameter7.ParameterName = "@usu";
                    mySqlParameter7.Value = text1;
                    cmd3.Parameters.Add(mySqlParameter7);

                    MySqlParameter mySqlParameter8 = new MySqlParameter();
                    mySqlParameter8.ParameterName = "@id3";
                    mySqlParameter8.Value = int.Parse(lblId.Content.ToString());
                    cmd3.Parameters.Add(mySqlParameter8);
                    connection.Open();
                    cmd3.ExecuteNonQuery();
                    break;
                default:
                    break;
            }
            MySqlCommand cmd4 = new MySqlCommand("SELECT id, nombre, apellidos, usuario, horasacum, horasmax FROM trabajadores", connection);
            DataTable dtact = new DataTable();
            dtact.Load(cmd4.ExecuteReader());
            connection.Close();
            dt = dtact;
            dtgTrabajadores.DataContext = dt;

            txt1.Visibility = Visibility.Collapsed;
            txt2.Visibility = Visibility.Collapsed;

            btnAceptar.Visibility = Visibility.Collapsed;
            btnHoras.Visibility = Visibility.Visible;
            btnNombre.Visibility = Visibility.Visible;
            btnUsuario.Visibility = Visibility.Visible;

            


        }
    }
}
